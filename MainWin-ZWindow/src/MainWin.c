#include "MainWin_internal.h"

typedef struct MainWin MainWin_t;

#define SELF		((MainWin_t*)pSelf)

static LPCTSTR const s_pszClass = TEXT("MainWin");


/*
	DECLARE ZWindowClientVtbl function...
*/

static HRESULT MySelfInit(
	void *pSelf
);
static void MySelfUninit(
	void *pSelf
);
static HRESULT MySelfHwndInit(
	void *pSelf, HWND hwndSelf
);
static void MySelfHwndUninit(
	void *pSelf
);
static BOOL MyHandleMsgLoopMsg(
	void *pSelf, MSG *pMsg
);
static BOOL MyHandleWndProc(
	void *pSelf, UINT msg, WPARAM w, LPARAM l,
	LRESULT *pl, BOOL const calledDefWndProc
);

static ZWindowClientVtbl_t const
s_wcv = {
	MySelfInit,
	MySelfUninit,
	MySelfHwndInit,
	MySelfHwndUninit,
	MyHandleMsgLoopMsg,
	MyHandleWndProc
};

/*
	DEFINE ZWindowClientVtbl function...
*/

static HRESULT MySelfInit(
	void *pSelf)
{
	SELF;
	return S_OK;
}

static void MySelfUninit(
	void *pSelf)
{
	SELF;
}

static HRESULT MySelfHwndInit(
	void *pSelf, HWND hwndSelf)
{
	UINT ec = 0;
	SELF->hwndSelf = hwndSelf;
	ec = MainWin_OnInit(SELF);
	if (ec != 0) {
		ZWindow_SetExitCode(SELF, ec);
		return E_FAIL;
	}
	return S_OK;
}

static void MySelfHwndUninit(
	void *pSelf)
{
	SELF;
}

static BOOL MyHandleMsgLoopMsg(
	void *pSelf, MSG *pMsg)
{
	SELF;  pMsg;
	return FALSE;
}

static BOOL MyHandleWndProc(
	void *pSelf, UINT msg, WPARAM w, LPARAM l,
	LRESULT *pl, BOOL const calledDefWndProc)
{
	SELF; msg; w; l; pl;
	if (calledDefWndProc) {}
	else
	{
		if (msg == WM_KEYDOWN) {
			if (w == VK_RETURN) {
				MainWin_ShowModal();
			}
		}
	}
	return FALSE;
}


/*
	DEFINE EXPORTED FUNCTION...
*/

EXTERN_C UINT MainWin_ShowModal(void)
{
	UINT ec = 0;
	HRESULT hr = 0;
	ZWindowCreateSpec_t wcs = { 0 };
	wcs.cx = 300;
	wcs.cy = 300;
	wcs.dwStyle = WS_OVERLAPPEDWINDOW;
	wcs.hbrBackground = GetSysColorBrush(COLOR_3DFACE);
	wcs.hCursor = LoadCursor(NULL, IDC_ARROW);
	wcs.pszClass = s_pszClass;
	wcs.x = CW_USEDEFAULT;
	hr = ZWindow_ShowModal(&wcs, &s_wcv, sizeof(MainWin_t), &ec);
	return ec;
}

