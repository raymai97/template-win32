#pragma once
#include "ZWindow.h"

#define MYSELF		((ZWindow_t*)(\
	(BYTE*)(pUsrSelf) - sizeof(ZWindow_t) ))

#define USRSELF		((void*)(\
	(BYTE*)(pSelf) + sizeof(ZWindow_t) ))

#define USRVTBL		(pSelf->wcv)


struct ZWindow {
	HWND hwndSelf;
	BOOL hasOwnMsgLoop;
	UINT usrExitCode;
	ZWindowClientVtbl_t wcv;
};
