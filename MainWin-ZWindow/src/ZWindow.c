#include "ZWindow_internal.h"

#define MY_SafeFree				APP_SafeFree
#define MY_CheckHrAndAssert		APP_CheckHrAndAssert


static HRESULT MyAllocSelf(
	SIZE_T cbUsrSelf, ZWindow_t **ppSelf, void **ppUsrSelf
);
static void MyFreeSelf(
	ZWindow_t *pSelf
);
static HRESULT MySelfInit(
	ZWindow_t *pSelf
);
static void MySelfUninit(
	ZWindow_t *pSelf
);
static HRESULT MySelfHwndInit(
	ZWindow_t *pSelf
);
static void MySelfHwndUninit(
	ZWindow_t *pSelf
);
static HRESULT MyCreateSelfHwnd(
	ZWindow_t *pSelf,
	ZWindowCreateSpec_t const *pWCS,
	HWND *pHwnd
);
static void MyDestroySelfHwnd(
	ZWindow_t *pSelf
);
static BOOL MyHandleMsgLoopMsg(
	ZWindow_t *pSelf, MSG *pMsg
);
static BOOL MyHandleWndProc(
	ZWindow_t *pSelf, UINT msg, WPARAM w, LPARAM l,
	LRESULT *pl, BOOL const calledDefWndProc
);
static HRESULT MyRunMsgLoop(
	ZWindow_t *pSelf
);
static ZWindow_t * MySelf(
	HWND hwnd,
	ZWindow_t *pNew
);


static HRESULT MyAllocSelf(
	SIZE_T cbUsrSelf, ZWindow_t **ppSelf, void **ppUsrSelf)
{
	SIZE_T const cbMySelf = sizeof(ZWindow_t);
	HRESULT hr = 0;
	ZWindow_t *pSelf = NULL;
	pSelf = MemAllocZero(cbMySelf + cbUsrSelf);
	if (!pSelf) {
		hr = E_OUTOFMEMORY; goto eof;
	}
	*ppSelf = pSelf;
	*ppUsrSelf = (BYTE*)pSelf + cbMySelf;
eof:
	return hr;
}

static void MyFreeSelf(
	ZWindow_t *pSelf)
{
	MemFree(pSelf);
}

static HRESULT MySelfInit(
	ZWindow_t *pSelf)
{
	HRESULT hr = 0;
	hr = USRVTBL.SelfInit(USRSELF);
	return hr;
}

static void MySelfUninit(
	ZWindow_t *pSelf)
{
	USRVTBL.SelfUninit(USRSELF);
}

static HRESULT MySelfHwndInit(
	ZWindow_t *pSelf)
{
	HRESULT hr = 0;
	hr = USRVTBL.SelfHwndInit(USRSELF, pSelf->hwndSelf);
	return hr;
}

static void MySelfHwndUninit(
	ZWindow_t *pSelf)
{
	USRVTBL.SelfHwndUninit(USRSELF);
}

static HRESULT MyCreateSelfHwnd(
	ZWindow_t *pSelf,
	ZWindowCreateSpec_t const *pWCS,
	HWND *pHwnd)
{
	HRESULT hr = 0;
	WNDCLASS wc = { 0 };
	HWND hwnd = NULL;
	wc.hbrBackground = pWCS->hbrBackground;
	wc.hCursor = pWCS->hCursor;
	wc.hIcon = pWCS->hIcon;
	wc.lpfnWndProc = ZWindow_WndProc;
	wc.lpszClassName = pWCS->pszClass;
	wc.lpszMenuName = pWCS->pszMenu;
	wc.style = pWCS->wcStyle;
	RegisterClass(&wc);
	hwnd = CreateWindowEx(
		pWCS->dwExStyle, pWCS->pszClass, NULL, pWCS->dwStyle,
		pWCS->x, pWCS->y, pWCS->cx, pWCS->cy,
		pWCS->hwndParent, pWCS->hMenu, NULL, pSelf
	);
	if (!IsWindow(hwnd)) {
		hr = HRESULT_FROM_WIN32(GetLastError());
		goto eof;
	}
	*pHwnd = hwnd;
	hr = MySelfHwndInit(pSelf);
eof:
	return hr;
}

static void MyDestroySelfHwnd(
	ZWindow_t *pSelf)
{
	if (IsWindow(pSelf->hwndSelf)) {
		DestroyWindow(pSelf->hwndSelf);
	}
	pSelf->hwndSelf = NULL;
}

static BOOL MyHandleMsgLoopMsg(
	ZWindow_t *pSelf, MSG *pMsg)
{
	return USRVTBL.HandleMsgLoopMsg(USRSELF, pMsg);
}

static BOOL MyHandleWndProc(
	ZWindow_t *pSelf, UINT msg, WPARAM w, LPARAM l,
	LRESULT *pl, BOOL const calledDefWndProc)
{
	BOOL ok = USRVTBL.HandleWndProc(USRSELF, msg, w, l, pl,
		calledDefWndProc);
	if (msg == WM_NCDESTROY
		&& calledDefWndProc
		&& pSelf->hasOwnMsgLoop)
	{
		PostQuitMessage(0);
	}
	return ok;
}

static HRESULT MyRunMsgLoop(
	ZWindow_t *pSelf)
{
	HRESULT hr = 0;
	int gm = 0;
	MSG msg = { 0 };
	pSelf->hasOwnMsgLoop = TRUE;
	while (gm = GetMessage(&msg, NULL, 0, 0), gm > 0)
	{
		if (!MyHandleMsgLoopMsg(pSelf, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}
	if (gm < 0) {
		hr = HRESULT_FROM_WIN32(GetLastError());
	}
	return hr;
}

static ZWindow_t * MySelf(
	HWND hwnd,
	ZWindow_t *pSelf)
{
	LPCSTR const pszprop = "SelfPtr";
	if (pSelf == INVALID_HANDLE_VALUE) {
		pSelf = RemovePropA(hwnd, pszprop);
	}
	else if (pSelf) {
		SetPropA(hwnd, pszprop, pSelf);
	}
	else {
		pSelf = GetPropA(hwnd, pszprop);
	}
	return pSelf;
}


EXTERN_C
HRESULT
ZWindow_ShowModal(
	ZWindowCreateSpec_t const *pWCSpec,
	ZWindowClientVtbl_t const *pVtbl,
	SIZE_T cbUsrSelf,
	UINT *pExitCode)
{
	HRESULT hr = 0;
	ZWindow_t *pSelf = NULL;
	void *pUsrSelf = NULL;
	HWND hwnd = NULL;

	hr = MyAllocSelf(cbUsrSelf, &pSelf, &pUsrSelf);
	MY_CheckHrAndAssert(pSelf && pUsrSelf);
	pSelf->wcv = *pVtbl;

	hr = MySelfInit(pSelf);
	if (FAILED(hr)) goto eof;

	hr = MyCreateSelfHwnd(pSelf, pWCSpec, &hwnd);
	MY_CheckHrAndAssert(IsWindow(hwnd));

	ShowWindow(hwnd, SW_SHOWDEFAULT);
	hr = MyRunMsgLoop(pSelf);
eof:
	if (pExitCode) {
		*pExitCode = pSelf->usrExitCode;
	}
	if (pSelf) {
		MyDestroySelfHwnd(pSelf);
		MySelfUninit(pSelf);
		MyFreeSelf(pSelf);
	}
	return hr;
}


EXTERN_C
LRESULT CALLBACK
ZWindow_WndProc(
	HWND hwnd, UINT msg, WPARAM w, LPARAM l)
{
	LRESULT lResult = 0;
	BOOL overriden = FALSE;
	ZWindow_t *pSelf = NULL;
	
	if (msg == WM_NCDESTROY) {
		pSelf = MySelf(hwnd, INVALID_HANDLE_VALUE);
	}
	else if (msg == WM_NCCREATE) {
		pSelf = ((CREATESTRUCT*)l)->lpCreateParams;
		pSelf->hwndSelf = hwnd;
		MySelf(hwnd, pSelf);
	}
	else {
		pSelf = MySelf(hwnd, NULL);
	}
	if (!pSelf) goto eof;
	
	overriden = MyHandleWndProc(pSelf, msg, w, l, &lResult, FALSE);
	if (overriden) goto eof;

	if (msg == WM_NCDESTROY) {
		MySelfHwndUninit(pSelf);
	}
eof:
	if (!overriden) {
		lResult = DefWindowProc(hwnd, msg, w, l);
		if (pSelf) {
			MyHandleWndProc(pSelf, msg, w, l, &lResult, TRUE);
		}
	}
	return lResult;
}

