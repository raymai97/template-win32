#include "ZWindow_internal.h"


EXTERN_C
void
ZWindow_SetExitCode(
	void *pUsrSelf,
	UINT exitCode)
{
	MYSELF->usrExitCode = exitCode;
}

