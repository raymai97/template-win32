#pragma once
#include "MainWin.h"
#include "ZWindow.h"

struct MainWin {
	HWND hwndSelf;
};


EXTERN_C UINT MainWin_OnInit(
	MainWin_t *pSelf
);

EXTERN_C void MainWin_OnUninit(
	MainWin_t *pSelf
);

EXTERN_C void MainWin_OnPaint(
	MainWin_t *pSelf
);
