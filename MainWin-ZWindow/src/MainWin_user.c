#include "MainWin_internal.h"

EXTERN_C UINT MainWin_OnInit(
	MainWin_t *pSelf)
{
	HWND const hwnd = pSelf->hwndSelf;
	SetWindowTextA(hwnd, "MainWindow");
	return 0;
}

EXTERN_C void MainWin_OnUninit(
	MainWin_t *pSelf)
{
	pSelf;
}

EXTERN_C void MainWin_OnPaint(
	MainWin_t *pSelf)
{
	pSelf;
}
