#pragma once
#include <Windows.h>

#define APP_SafeFree(p, fn) \
	if (p) { fn(p); (p) = NULL; }

#define APP_CheckHrAndAssert(x) \
	if (FAILED(hr)) goto eof; \
	if (!(x)) { hr = E_UNEXPECTED; goto eof; }


EXTERN_C LPVOID MemAllocZero(SIZE_T cb);
EXTERN_C void MemFree(LPVOID ptr);

EXTERN_C UINT App_Init(void);
EXTERN_C void App_Uninit(void);

