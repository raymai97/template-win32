#include "ZWindow.h"

typedef struct ZWinDemo ZWinDemo_t;

#define SELF		((ZWinDemo_t*)pSelf)

static LPCTSTR const s_pszClass = TEXT("ZWinDemo");

struct ZWinDemo {
	HWND hwndSelf;
};


/*
	DECLARE ZWindowClientVtbl function...
*/

static HRESULT MySelfInit(
	void *pSelf
);
static void MySelfUninit(
	void *pSelf
);
static HRESULT MySelfHwndInit(
	void *pSelf, HWND hwndSelf
);
static void MySelfHwndUninit(
	void *pSelf
);
static BOOL MyHandleMsgLoopMsg(
	void *pSelf, MSG *pMsg
);
static BOOL MyHandleWndProc(
	void *pSelf, UINT msg, WPARAM w, LPARAM l,
	LRESULT *pl, BOOL const calledDefWndProc
);

static ZWindowClientVtbl_t const
s_WCV = {
	MySelfInit,
	MySelfUninit,
	MySelfHwndInit,
	MySelfHwndUninit,
	MyHandleMsgLoopMsg,
	MyHandleWndProc
};

/*
	DEFINE ZWindowClientVtbl function...
*/

static HRESULT MySelfInit(
	void *pSelf)
{
	SELF;
	return S_OK;
}

static void MySelfUninit(
	void *pSelf)
{
	SELF;
}

static HRESULT MySelfHwndInit(
	void *pSelf, HWND hwndSelf)
{
	SELF->hwndSelf = hwndSelf;
	return S_OK;
}

static void MySelfHwndUninit(
	void *pSelf)
{
	SELF;
}

static BOOL MyHandleMsgLoopMsg(
	void *pSelf, MSG *pMsg)
{
	SELF; pMsg;
	return S_FALSE;
}

static BOOL MyHandleWndProc(
	void *pSelf, UINT msg, WPARAM w, LPARAM l,
	LRESULT *pl, BOOL const calledDefWndProc)
{
	SELF; msg; w; l; pl; calledDefWndProc;
	return FALSE;
}


/*
	DEFINE EXPORTED FUNCTION...
*/

