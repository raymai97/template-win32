#pragma once
#include <Windows.h>

#ifndef APP_SafeFree
#define APP_SafeFree(p, fn) \
	if (p) { fn(p); (p) = NULL; }
#endif

#ifndef APP_CheckHrAndAssert
#define APP_CheckHrAndAssert(x) \
	if (FAILED(hr)) goto eof; \
	if (!(x)) { hr = E_UNEXPECTED; goto eof; }
#endif

EXTERN_C LPVOID MemAllocZero(SIZE_T cb); // userimpl
EXTERN_C void MemFree(LPVOID ptr); // userimpl


typedef struct ZWindow ZWindow_t;
typedef struct ZWindowClientVtbl ZWindowClientVtbl_t;
typedef struct ZWindowCreateSpec ZWindowCreateSpec_t;

struct ZWindowClientVtbl {
	// Called before SelfHwnd created
	HRESULT(*SelfInit)(void *pSelf);

	// Called after SelfHwnd destroyed
	void(*SelfUninit)(void *pSelf);

	// Called after CreateWindowEx() succeeded
	HRESULT(*SelfHwndInit)(void *pSelf, HWND hwndSelf);

	// Called when WM_NCDESTROY
	// Can be overriden by HandleWndProc
	void(*SelfHwndUninit)(void *pSelf);

	// Return TRUE to override default
	BOOL(*HandleMsgLoopMsg)(void *pSelf, MSG *pMsg);

	// Return TRUE to override default
	BOOL(*HandleWndProc)(void *pSelf, UINT msg, WPARAM w, LPARAM l,
		LRESULT *plResult, BOOL const calledDefWndProc);
};

struct ZWindowCreateSpec {
	/* Window Class */
	UINT		wcStyle;
	HICON		hIcon;
	HCURSOR		hCursor;
	HBRUSH		hbrBackground;
	LPCTSTR		pszMenu;
	LPCTSTR		pszClass;
	/* Window Default */
	LPCTSTR		pszTitle;
	DWORD		dwStyle;
	DWORD		dwExStyle;
	int			x, y, cx, cy;
	HWND		hwndParent;
	HMENU		hMenu;
};


EXTERN_C
void
ZWindow_SetExitCode(
	void *pSelf,
	UINT exitCode
);

EXTERN_C
HRESULT
ZWindow_ShowModal(
	ZWindowCreateSpec_t const *pWCSpec,
	ZWindowClientVtbl_t const *pVtbl,
	SIZE_T cbUserSelf,
	UINT *pExitCode /*opt*/
);

EXTERN_C
LRESULT CALLBACK
ZWindow_WndProc(
	HWND hwnd, UINT msg, WPARAM w, LPARAM l
);
