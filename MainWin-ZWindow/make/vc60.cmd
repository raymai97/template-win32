@echo off
setLocal enableDelayedExpansion
call D:\vc60\vcvar.cmd || goto ggEnv
pushd %~dp0
cd..\src
set srcDir=!cd!
set outDir=%~dp0

set c_define=-DWIN32_LEAN_AND_MEAN
call :make vc60_ansi
set c_define=-DUNICODE !cDefine!
call :make vc60_wide
popd
exit/b

:make
echo.
echo. Making %~1
echo.
cd !srcDir!
for %%i in (*.c) do (
set /p z=Compiling: <nul
cl -nologo -O1 -MD -c %%i ^
	-Fo"!outDir!\%~1_%%i.obj" ^
	!c_define!	|| goto gg
)
cd !outDir!
link -nologo %~1_*.obj ^
	-out:%~1.exe ^
	-entry:RawMain ^
	-fixed:NO ^
	-release ^
	-subsystem:Windows ^
	-opt:nowin98 ^
	kernel32.lib ^
	user32.lib || goto gg
del /q %~1_*.obj
exit/b

:ggEnv
echo MSVC 6.0 build env not ready
goto gg

:gg
pause
popd
exit/b 1
