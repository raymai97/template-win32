#pragma once
#include "MainWin.h"

struct MainWin {
	HWND hwndSelf;
	HWND hwndFocus;
	BOOL hasOwnMsgLoop;
};
