#include "App_internal.h"


static App_t s_App;


EXTERN_C LPVOID MemAllocZero(SIZE_T cb)
{
	LPVOID ptr = HeapAlloc(GetProcessHeap(), 0, cb);
	if (ptr) { ZeroMemory(ptr, cb); }
	return ptr;
}

EXTERN_C void MemFree(LPVOID ptr)
{
	HeapFree(GetProcessHeap(), 0, ptr);
}


EXTERN_C UINT App_Init(void)
{
	return 0;
}

EXTERN_C void App_Uninit(void)
{
}
