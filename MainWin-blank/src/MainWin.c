#include "MainWin_internal.h"

#define MY_BehaveLikeDialog		(1)


static LPCTSTR const s_pszClass = TEXT("MainWin");


static UINT MyCreateSelfHwnd(
	MainWin_t *pSelf
);
static void MyDestroySelfHwnd(
	MainWin_t *pSelf
);
static UINT MyRunMsgLoop(
	MainWin_t *pSelf
);
static MainWin_t * MySelf(
	HWND hwnd, MainWin_t *pNew
);
static UINT MySelfInit(
	MainWin_t *pSelf
);
static void MySelfUninit(
	MainWin_t *pSelf
);
static UINT MySelfHwndInit(
	MainWin_t *pSelf
);
static void MySelfHwndUninit(
	MainWin_t *pSelf
);
static LRESULT CALLBACK MyWndProc(
	HWND hwnd, UINT msg, WPARAM w, LPARAM l
);



EXTERN_C UINT MainWin_ShowModal(void)
{
	UINT ret = 0;
	MainWin_t self = { 0 };
	ret = MySelfInit(&self);
	if (ret) goto eof;
	ret = MyCreateSelfHwnd(&self);
	if (ret) goto eof;
	ShowWindow(self.hwndSelf, SW_SHOWDEFAULT);
	ret = MyRunMsgLoop(&self);
eof:
	MyDestroySelfHwnd(&self);
	MySelfUninit(&self);
	return ret;
}



static UINT MyCreateSelfHwnd(
	MainWin_t *pSelf)
{
	UINT ret = 0;
	WNDCLASS wc = { 0 };
	HWND hwnd = NULL;
	wc.lpfnWndProc = MyWndProc;
	wc.lpszClassName = s_pszClass;
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = GetSysColorBrush(COLOR_3DFACE);
	wc.style = CS_HREDRAW | CS_VREDRAW;
	RegisterClass(&wc);
	hwnd = CreateWindowEx(0,
		s_pszClass, NULL,
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, 0, 300, 300,
		NULL, NULL, NULL, pSelf);
	if (!hwnd) {
		ret = 1; goto eof;
	}
	pSelf->hwndSelf = hwnd;
	ret = MySelfHwndInit(pSelf);
eof:
	if (ret) {
		MyDestroySelfHwnd(pSelf);
	}
	return ret;
}

static void MyDestroySelfHwnd(
	MainWin_t *pSelf)
{
	if (IsWindow(pSelf->hwndSelf)) {
		DestroyWindow(pSelf->hwndSelf);
	}
	pSelf->hwndSelf = NULL;
}

static UINT MyRunMsgLoop(
	MainWin_t *pSelf)
{
	HWND const hwnd = pSelf->hwndSelf;
	MSG msg = { 0 };
	pSelf->hasOwnMsgLoop = TRUE;
	while (GetMessage(&msg, NULL, 0, 0))
	{
#if MY_BehaveLikeDialog
		if (!IsDialogMessage(hwnd, &msg))
#endif
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}
	return (UINT)(msg.wParam);
}

static MainWin_t * MySelf(
	HWND hwnd, MainWin_t *pNew)
{
	LPCSTR const pszprop = "SelfPtr";
	MainWin_t *pSelf = pNew;
	if (pNew == INVALID_HANDLE_VALUE) {
		pSelf = RemovePropA(hwnd, pszprop);
	}
	else if (pNew) {
		SetPropA(hwnd, pszprop, pNew);
	}
	else {
		pSelf = GetPropA(hwnd, pszprop);
	}
	return pSelf;
}

// Called when hwndSelf == NULL
// Return value for ExitProcess() if non-zero
static UINT MySelfInit(
	MainWin_t *pSelf)
{
	pSelf;
	return 0;
}

// Called after WM_NCDESTROY
static void MySelfUninit(
	MainWin_t *pSelf)
{
	pSelf;
}

// Called when hwndSelf != NULL
// Return value for ExitProcess() if non-zero
static UINT MySelfHwndInit(
	MainWin_t *pSelf)
{
	HWND const hwnd = pSelf->hwndSelf;
	SetWindowTextA(hwnd, "MainWindow");
	return 0;
}

// Called when WM_NCDESTROY
static void MySelfHwndUninit(
	MainWin_t *pSelf)
{
	pSelf;
}

static LRESULT CALLBACK MyWndProc(
	HWND hwnd, UINT msg, WPARAM w, LPARAM l)
{
	LRESULT lResult = 0;
	BOOL overriden = FALSE;
	MainWin_t *pSelf = NULL;
	if (msg == WM_NCDESTROY) {
		pSelf = MySelf(hwnd, INVALID_HANDLE_VALUE);
	}
	else if (msg == WM_NCCREATE) {
		pSelf = ((CREATESTRUCT*)l)->lpCreateParams;
		MySelf(hwnd, pSelf);
	}
	else {
		pSelf = MySelf(hwnd, NULL);
	}
	if (!pSelf) goto eof;
	if (msg == WM_NCDESTROY)
	{
		MySelfHwndUninit(pSelf);
		if (pSelf->hasOwnMsgLoop) {
			PostQuitMessage(0);
		}
	}
eof:
	if (!overriden)
	{
		lResult = DefWindowProc(hwnd, msg, w, l);
	}
	return lResult;
}

