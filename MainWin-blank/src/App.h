#pragma once
#include <Windows.h>

#define APP_SafeFree(p, fn) \
	if (p) { fn(p); (p) = NULL; }

EXTERN_C LPVOID MemAllocZero(SIZE_T cb);
EXTERN_C void MemFree(LPVOID ptr);

EXTERN_C UINT App_Init(void);
EXTERN_C void App_Uninit(void);
