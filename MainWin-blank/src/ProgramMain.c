#include "App.h"
#include "MainWin.h"

EXTERN_C UINT ProgramMain(void)
{
	UINT ret = App_Init();
	if (ret) goto eof;
	ret = MainWin_ShowModal();
eof:
	App_Uninit();
	return ret;
}

EXTERN_C void APIENTRY RawMain(void)
{
	ExitProcess(ProgramMain());
}

int APIENTRY WinMain(HINSTANCE z1, HINSTANCE z2, LPSTR z3, int z4)
{
	z1; z2; z3; z4;
	return ProgramMain();
}

int main(void)
{
	return ProgramMain();
}
